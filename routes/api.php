<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', array('as' => 'test', 'uses' => 'TestController@Consume'));
Route::post('/documentsGeneration', array('as' => 'documentsGeneration', 'uses' => 'ProdoctivityController@DocumentsGenaration'));
Route::get('/generationExample', array('as' => 'generationExample',   'uses' => 'ProdoctivityController@GetGenerationExample'));
Route::post('/documents', array('as' =>'getDocuments', 'uses' => 'OnBaseController@getDocuments'));
Route::post('newChild', array('as' =>'newChild', 'uses' => 'WorkflowController@newChildEntityRecord'));