<?php

namespace App\Traits;

/**
 *
 */
trait DecodeDocuments
{
    /**
     * Description: Create grid's array to send.
     * @param $data is the data that containt.
     * @return array
     */
    public function DecodeDocuments($documents)
    {
        //dd($documents);
        $finaldocuments = [];
        foreach($documents as $document)
        {
            //dd($document['documentType']['name']);
            $current = [
                        
                            [
                                'key' => 'iddocumento',
                                'value' => $document['documentHandler']
                            ],
                            [
                                'key' => 'nombredocumento',
                                'value' => $document['name']
                            ],
                            [
                                'key' => 'tipodocumento',
                                'value' => $document['documentType']['name']
                            ]
                                                   
                    ];
            array_push($finaldocuments,$current);
        }
        return $finaldocuments;
    }
}
