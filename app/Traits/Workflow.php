<?php

namespace App\Traits;

/**
 *
 */
trait Workflow
{
    /**
     * Description: Create grid's array to send.
     * @param $data is the data that containt.
     * @return array
     */
    public function newChildEntityRecord($entityParamGrid)
    {
    
        $wf = new \nusoap_client(env('URL_SOFTEXPERT'), false);//Esta url es la de la webservice de Workflow, se captura desde un archivo de configuracion (.env)
        $wf->setCredentials(env('SE_USER'), env('SE_PASSWORD'));  
        $wf->soap_defencoding = 'UTF-8';
        $wf->decode_utf8 = false;
        $response = $wf->call('newChildEntityRecordList', $entityParamGrid);
       
        $response = ['Status' => $response['Status'], 'Detail' => utf8_encode($response['Detail'])];
        
        return $response;
    }
}
