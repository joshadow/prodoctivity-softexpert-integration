<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Traits\DecodeDocuments;
use App\Traits\Workflow;
use App\Traits\GridSoftExpert;

class OnBaseController extends Controller
{
    use DecodeDocuments;
    use Workflow;
    use GridSoftExpert;
    public function createOnBaseClient()
    {
        
        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json',
                           'Authorization' => 'Basic ' . env('ON_BASE_TOKEN')
                        ]
        ]);
       
        return $client;
    }

    public function getDocuments(Request $request){
        //dd($request->all());
        $client = $this->createOnBaseClient('filter');
        $url = "http://18.222.168.20:8093/api/documents/filter";
        $params = [
                    'documentTypes' =>
                        [
                            [
                                'name' => 'Identificacion'
                            ]
                        ],
                        'keywords' =>
                            [[
                            'keywordType' =>
                                [
                                    'name' => $request->keyword
                                ],
                                'value' => $request->value
                        ],
                       
                    ],
                    'includeBinaries' => false
                ];
    $tempBody = '{
        "documentTypes": [
            {
                "name": "Identificacion"
            }
        ],
        "keywords": [
            {
                "keywordType": {
                    "name": "Número de solicitud"
                },
                "value": "123654"
            }
        ],
        "includeBinaries": false
    }';

    $params = json_encode($params);
    
    $apiRequest = $client->post($url,[
          'body' => $params
      ]);
    
     
    $response = $apiRequest->getBody();
    $encoded = json_decode($response,true);
    //dd($encoded);
    $documents = $this->DecodeDocuments($encoded);
    $data = $this->arrayGrid($documents);
    
    $gridparams = $this->buildParamToSendGrid($request->relationship,$request->entity,$request->value,$data);
        
    return $this->newChildEntityRecord($gridparams);
    
  }
}
