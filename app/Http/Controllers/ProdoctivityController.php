<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ProdoctivityController extends Controller
{
    public function DocumentsGenaration(Request $request)
    {
        $client = $this->createProdoctivityClient('documentsgenerationrequest');
        $keywords = $request->keywords;
        $records = $request->records;

        $params = [
            'contexts' => [
                [
                    'data'=> [
                        [
                            'keywords' => $keywords,
                            'records'  => $records
                        ]
                    ],
                    'templatesHandleList' => [$request->template]
                ]
            ],
            'templatesHandleList' => [$request->template],
            'version' => 'LatestVersion',
            'preview' => false,
            'resultType' => 'InlineSoap'
        ];
       
        $apiRequest = $client->post($url,[
            'body' => json_encode($params)
        ]);
      

        $response = $apiRequest->getBody();
        $encoded = json_decode($response,true);

        return $encoded;
       
    }

    public function createProdoctivityClient()
    {
        $url = "http://18.222.168.20/CoordinatorFront/api/v1/prodoctivityapi/";
        //dd($url);
        $credentials = base64_encode('manager:password');
        $client = new Client([
            'headers' => [ 'Content-Type' => 'application/json',
                           'Authorization' => 'Basic ' . $credentials,
                           'x-api-key' => 'b6fe787d-b44f-4dfe-83f1-5b98063ad028'
                        ]
        ]);

        return $client;
    }
    public function GetGenerationExample()
    {
        $method = 'templates/1/generation-example';
        $client = $this->createProdoctivityClient($method);
        $apiRequest = $client->get($url);
        $response = $apiRequest->getBody();

        $encoded = json_encode($response,true);
  

        return $response;
    }


    
}
