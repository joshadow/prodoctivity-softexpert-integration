<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    //

    public function Consume()
    {
        $url = "https://my-json-server.typicode.com/typicode/demo/posts";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET',$url);
        $body = $response->getBody();
        $content = $body->getContents();

        $arr = json_decode($content);
        return $arr;
    }
}
